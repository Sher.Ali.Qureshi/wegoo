<?php
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return view('welcome');
});
Route::get('signUp', function () {
    return view('pages.signUp');
});

Route::get('test', function () {
    return view('pages.test');
});

Route::get('test2', function () {
    return view('pages.test2');
});

Route::get('uber', function () {
    return view('pages.uber');
});

Route::get('driverlicense', function () {
    return view('pages.driverlicense');
});

Route::get('landingPage', function () {
    return view('pages.landingPage');
});

Route::get('business', function () {
    return view('pages.business');
});

Route::get('drivers', function () {
    return view('pages.drivers');
});

Route::get('help', function () {
    return view('pages.help');
});

Route::post('registerUser', 'UserController@registerUser');
Route::post('license', 'UserController@license');



//////////////////////////////Admin Route///////////////////////
Route::get('index', function () {
    return view('adminPages.index');
});
Route::get('admin', function () {
    return view('adminPages.admin');
});



Route::get('driver', function () {
	$user=User::paginate(10);
    return view('adminPages.driver')->with('user',$user);
});

Route::post('driver', 'AdminController@driver');
Route::get('delete-user/{id}', 'AdminController@deleteUser');
Route::get('detail-user/{id}', 'AdminController@detailUser');
Route::post('registerAdmin', 'AdminController@registerAdmin');
Route::post('login', 'AdminController@login');