<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="UTF-8">
    <title>Wegoo</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href=""/>

    <!--global styles-->
    <link rel="stylesheet" href="../css/components.css" />
    <link rel="stylesheet" href="../css/custom.css" />
    <!-- end of global styles-->
    <link rel="stylesheet" href="../vendors/chartist/css/chartist.min.css" />
    <link rel="stylesheet" href="../vendors/circliful/css/jquery.circliful.css">
    <link rel="stylesheet" href="../css/pages/index.css">
    <link rel="stylesheet" href="#" id="skin_change" />

</head>

<body>
<div class="preloader" style=" position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 100000;
  backface-visibility: hidden;
  background: #ffffff;">
    <div class="preloader_img" style="width: 200px;
  height: 200px;
  position: absolute;
  left: 48%;
  top: 48%;
  background-position: center;
z-index: 999999">
        <img src="../img/loader.gif" style=" width: 40px;" alt="loading...">
    </div>
</div>
<div id="wrap">
    <div id="top">
        <!-- .navbar -->
        <nav class="navbar navbar-static-top">
            <div class="container-fluid m-0">
                <a class="navbar-brand mr-0" href="index">
                    <h4 class="text-white"><img src="../image/logo.png" class="admin_img" alt="logo"> WEGOO ADMIN</h4>
                </a>
                <div class="menu mr-sm-auto">
                        <span class="toggle-left" id="menu-toggle">
                        <i class="fa fa-bars text-white"></i>
                    </span>
                </div>
                <div class="navbar-toggleable-sm m-lg-auto d-none d-lg-flex top_menu" id="nav-content">
                    
                </div>
                <div class="topnav dropdown-menu-right ml-auto">
                    <div class="btn-group">
                        <div class="notifications no-bg">
                            
                            <div class="dropdown-menu drop_box_align" role="menu" id="messages_dropdown">
                                
                                
                                
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="btn-group">
                        <div class="user-settings no-bg">
                            <button type="button" class="btn btn-default no-bg micheal_btn" data-toggle="dropdown">
                                <img  class="admin_img2 rounded-circle avatar-img" > <strong>Admin</strong>
                                <span class="fa fa-sort-down white_bg"></span>
                            </button>
                            <div class="dropdown-menu admire_admin">
                                <div class="popover-header">Wegoo Admin</div>
                                <a class="dropdown-item" href="edit_user.html"><i class="fa fa-cogs"></i>
                                    Settings</a>
                                <a class="dropdown-item" href="login2.html"><i class="fa fa-sign-out"></i>
                                    Log Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </nav>
        <!-- /.navbar -->
        <!-- /.head -->
    </div>
    <!-- /#top -->
    <div class="wrapper">
        <div id="left">
            <div class="menu_scroll">
                
                <!-- #menu -->
                <ul id="menu">
                    
                    <li>
                        <a href="{{url('/index')}}">
                            <i class="fa fa-tachometer"></i>
                            <span class="link-title menu_hide">&nbsp;Dashboard

                                </span>
                        </a>
                    </li>
                     <li>
                        <li>
                        <a href="{{url('/driver')}}">
                            <i class="fa fa-home"></i>
                            <span class="link-title menu_hide">&nbsp;Driver</span>
                        </a>
                    </li>
                    
                    </ul>
                <!-- /#menu -->
            </div>
        </div>
        <!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                    <div class="row">
                        <div class="col-6">
                            <h4 class="m-t-5">
                                <i class="fa fa-user"></i>
                                Driver Detail
                            </h4>
                        </div>
                    </div>
                </div>
            </header>
            
            <div class="outer">
                <div class="inner bg-container">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 m-t-35">
                                    <div class="text-center">
                                        <div class="form-group">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumb_zoom zoom admin_img_width">
                                                    <label style="margin-top: 4%"><strong> User Licence Picture</strong></label>
                                                    <br>
                                                    <img src="{{$user->license}}" style="width: 140%; height: 120%;; margin-left: 5%"></div>
                                                <div class="fileinput-preview fileinput-exists thumb_zoom zoom admin_img_width"></div>
                                                <div class="btn_file_position">
                                                    
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-lg-6 m-t-25">
                                    <div>
                                        <ul class="nav nav-inline view_user_nav_padding">
                                            <li class="nav-item card_nav_hover">
                                                <a class="nav-link active" href="#user" id="home-tab"
                                                   data-toggle="tab" aria-expanded="true">User Details</a>
                                            </li>
                                        </ul>
                                        <div id="clothing-nav-content" class="tab-content m-t-10">
                                            <div role="tabpanel" class="tab-pane fade show active" id="user">
                                                <table class="table" id="users">
                                                    <tr>
                                                        <td>First Name</td>
                                                        <td class="inline_edit">
                                                        <span class="editable"
                                                              data-title="Edit User Name">{{$user->firstName}}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Last Name</td>
                                                        <td class="inline_edit">
                                                        <span class="editable"
                                                              data-title="Edit User Name">{{$user->lastName}}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Last Name</td>
                                                        <td>
                                                            <span class="editable" 
                                                            data-title="Edit E-mail">{{$user->email}}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Phone Number</td>
                                                        <td>
                                                            <span class="editable" data-title="Edit Phone Number">{{$user->phoneNumber}}</span>
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td>City</td>
                                                        <td>
                                                            <span class="editable" data-title="Edit City">{{$user->city}}</span>
                                                        </td>
                                                    </tr>
                                                    
                                                </table>
                                            </div>
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>

        

  </div>

  
</div>
                                        
                                    </div>
                                </div>
                        
            <!-- /#content -->
        </div>
    </div>
    <!--wrapper-->
    
    <!-- # right side -->
</div>
<!-- /#wrap -->
<!-- global scripts-->
<script src="../js/components.js"></script>
<script src="../js/custom.js"></script>
<!--end of global scripts-->
<!--  plugin scripts -->
<script src="../vendors/countUp.js/js/countUp.min.js"></script>
<script src="../vendors/flip/js/jquery.flip.min.js"></script>
<script src="../js/pluginjs/jquery.sparkline.js"></script>
<script src="../vendors/chartist/js/chartist.min.js"></script>
<script src="../js/pluginjs/chartist-tooltip.js"></script>
<script src="../vendors/swiper/js/swiper.min.js"></script>
<script src="../vendors/circliful/js/jquery.circliful.min.js"></script>
<script src="../vendors/flotchart/js/jquery.flot.js" ></script>
<script src="../vendors/flotchart/js/jquery.flot.resize.js"></script>
<!--end of plugin scripts-->

<script src="../js/pages/index.js"></script>

</body>
</html>
