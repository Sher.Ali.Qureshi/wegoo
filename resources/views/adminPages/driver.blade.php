<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="UTF-8">
    <title>Wegoo</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href=""/>

    <!--global styles-->
    <link rel="stylesheet" href="css/components.css" />
    <link rel="stylesheet" href="css/custom.css" />
    <!-- end of global styles-->
    <link rel="stylesheet" href="vendors/chartist/css/chartist.min.css" />
    <link rel="stylesheet" href="vendors/circliful/css/jquery.circliful.css">
    <link rel="stylesheet" href="css/pages/index.css">
    <link rel="stylesheet" href="#" id="skin_change" />


</head>

<body>
<div class="preloader" style=" position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 100000;
  backface-visibility: hidden;
  background: #ffffff;">
    <div class="preloader_img" style="width: 200px;
  height: 200px;
  position: absolute;
  left: 48%;
  top: 48%;
  background-position: center;
z-index: 999999">
        <img src="img/loader.gif" style=" width: 40px;" alt="loading...">
    </div>
</div>
<div id="wrap">
    <div id="top">
        <!-- .navbar -->
        <nav class="navbar navbar-static-top">
            <div class="container-fluid m-0">
                <a class="navbar-brand mr-0" href="index.html">
                    <h4 class="text-white"><img src="image/logo.png" class="admin_img" alt="logo"> WEGOO ADMIN</h4>
                </a>
                <div class="menu mr-sm-auto">
                        <span class="toggle-left" id="menu-toggle">
                        <i class="fa fa-bars text-white"></i>
                    </span>
                </div>
                <div class="navbar-toggleable-sm m-lg-auto d-none d-lg-flex top_menu" id="nav-content">
                    
                </div>
                <div class="topnav dropdown-menu-right ml-auto">
                    <div class="btn-group">
                        <div class="notifications no-bg">
                            
                            <div class="dropdown-menu drop_box_align" role="menu" id="messages_dropdown">
                                
                                
                                
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="btn-group">
                        <div class="user-settings no-bg">
                            <button type="button" class="btn btn-default no-bg micheal_btn" data-toggle="dropdown">
                                <img  class="admin_img2 rounded-circle avatar-img" > <strong>Admin</strong>
                                <span class="fa fa-sort-down white_bg"></span>
                            </button>
                            <div class="dropdown-menu admire_admin">
                                <div class="popover-header">Wegoo Admin</div>
                                <a class="dropdown-item" href="edit_user.html"><i class="fa fa-cogs"></i>
                                    Settings</a>
                                <a class="dropdown-item" href="login2.html"><i class="fa fa-sign-out"></i>
                                    Log Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </nav>
        <!-- /.navbar -->
        <!-- /.head -->
    </div>
    <!-- /#top -->
    <div class="wrapper">
        <div id="left">
            <div class="menu_scroll">
                
                <!-- #menu -->
                <ul id="menu">
                    
                    <li>
                        <a href="index">
                            <i class="fa fa-tachometer"></i>
                            <span class="link-title menu_hide">&nbsp;Dashboard

                                </span>
                        </a>
                    </li>
                     <li>
                        <li class="active">
                        <a href="driver">
                            <i class="fa fa-home"></i>
                            <span class="link-title menu_hide">&nbsp;Driver</span>
                        </a>
                    </li>
                    
                    </ul>
                <!-- /#menu -->
            </div>
        </div>
        <!-- /#left -->
        <div id="content" class="bg-container">
            
            <div class="outer">
                    <div class="inner bg-container">
                        <div class="card">
                            <div class="card-header bg-white">
                                User Grid
                            </div>
                            <div class="card-body m-t-35" id="user_body">
                                <div class="table-toolbar">
                                    
                                    <div class="btn-group float-right users_grid_tools">
                                        <div class="tools"></div>
                                    </div>
                                </div>
                                <div>
                                    <div>
                                        <table class="table  table-striped table-bordered table-hover dataTable no-footer" id="editable_table" role="grid">
                                            <thead>
      <tr bgcolor="#f7f7f7" align="left">
        <th>First</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Phone NO.</th>
        <th>City</th>
        <th>Detail</th>
        <th>Delete</th>
      </tr>
      </thead>
      <tbody>
@foreach($user as $key)                                       
        <tr>
        <td>{{$key->firstName}}</td>
        <td>{{$key->lastName}}</td>
        <td>{{$key->email}}</td>
        <td>{{$key->phoneNumber}}</td>
        <td>{{$key->city}}</td>                                         
        <td><a href="detail-user/{{$key['id']}}" style="text-decoration: none"><span class="btn btn-primary">Detail</span></a></td>
        <td><a href="delete-user/{{$key['id']}}" style="text-decoration: none"><span class="btn btn-danger">Delete</span></a></td>
        </tbody>
    @endforeach

                                           
    </table>
                                    </div>
                                    
                                        <div class="col-lg-12">
                                            <ul class="pagination pagination_padding_general_components">
                                                <li class="page-item">
                                                
                                                {{ $user->links() }}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            

                        </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                    </div>
                    <!-- /.inner -->
                </div>
                <!-- /.outer -->
            

  </div>

  
</div>
                                        
                                    </div>
                                </div>
                        
            <!-- /#content -->
        </div>
    </div>

    <!--wrapper-->
    
    <!-- # right side -->
</div>
<!-- /#wrap -->
<!-- global scripts-->
<script src="js/components.js"></script>
<script src="js/custom.js"></script>
<!--end of global script-->
<!--  plugin scripts -->
<script src="vendors/countUp.js/js/countUp.min.js"></script>
<script src="vendors/flip/js/jquery.flip.min.js"></script>
<script src="js/pluginjss/jquery.sparkline.js"></script>
<script src="vendors/chartist/js/chartist.min.js"></script>
<script src="js/pluginjs/chartist-tooltip.js"></script>
<script src="vendors/swiper/js/swiper.min.js"></script>
<script src="vendors/circliful/js/jquery.circliful.min.js"></script>
<script src="vendors/flotchart/js/jquery.flot.js" ></script>
<script src="vendors/flotchart/js/jquery.flot.resize.js"></script>
<!--end of plugin scripts-->

<script src="js/pages/index.js"></script>

</body>
</html>
