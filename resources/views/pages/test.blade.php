<!DOCTYPE html>
<html>
  <head>
    <title>Wegoo</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/w3css/3/w3.css">
    <link href="css/styles.css" rel="stylesheet">
<style>
   body  {
    background-image: url("image/background.png");
    background-position: center center;
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-size: cover;
    width: auto;
    height: auto;
}

</style>
<script>
                    $(document).ready(function(){
                        @isset($Error)
                            alert("{{$Error}}");
                            console.log("{{$Error}}");
                        
                        @endisset
                    });
                </script>
</head>
<body>
  <div class="col-12 col-md-12 col-sm-12">
      <center><img class="img1" src="image/logo.png" ></center>
      <br>
      <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-2">
      </div>
      <div class="col-md-6 col-sm-8 col-xs-3">
        <div class="header">
         <center><h4>Join Now</h4></center>   
        </div>
         <div class="row middle">
        <div  class="col-md-6 col-sm-6 col-xs-6">
          <br>
        <form class="pure-form" role="form" method="POST" action="registerUser" >
        <input class="normal" type="text"  name="firstName" placeholder="First Name" required>
        <br><br>
        <input class="normal" type="text" name="lastName" placeholder="Last Name" required>
        <br><br>
        <input class="normal" type="email" name="email" placeholder="Email" required>
        <br><br>
        <input class="normal" type="text" name="phoneNumber" placeholder="Phone Number" required>
       <br><br>
        <input class="normal" type="password" placeholder="Password" name="password" id="password" required>
        <br><br>
        <input class="normal" type="password" placeholder="Confirm Password" id="confirm_password"  required>
        <br><br>
        <input class="normal" type="text" name="city" placeholder="City" required>
        <br><br>
        <input class="normal" type="text"  name="code" placeholder="Invite Code (Optional)">
        <br><br>
       </div>
       <div class="col-md-6 col-sm-6 col-xs-6" style="margin-top: 10%">
            <h5> Have an Account?  <strong style="color: #388ccc">Log In</strong></h5>
            <h6>
                By proceeding, I agree that Wegoo or its representatives may contact me by email, or SMS (including by automated mears) at the email address or number I provide, including for marketing purposes. I have read and understand the<strong style="color: #388ccc">  Driver Privacy Statement</strong>
            </h6>
            <br>
              <h6><center>@ Wegoo Technologes.Inc</center></h6>
            <center><h6 style="color: #388ccc">
                Privacy   Terms
                </h6></center>
          </div>
               
            
        </div>
        <button type="submit" class="btn1 btn-primary btn-block" ><center><h4>Next</h4></center></button>
       </div>
       {!! csrf_field() !!}
       </form>
       </center> 
        </div>

      </div>
      </div>
      
  </div>
  <script type="text/javascript">

  var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>



  
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </body>
</html>