<!DOCTYPE html>
<html>
  <head>
    <title>Wegoo</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/w3css/3/w3.css">
    <link href="css/styles.css" rel="stylesheet">
<style>
   body  {
    background-image: url("image/background.png");
    background-position: center center;
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-size: cover;
    width: auto;
    height: auto;
}

</style>
</head>
<body>
  <div class="col-12 col-md-12 col-sm-12">
      <center><img class="img1" src="image/Logo2.png" ></center>
      <br>
      <div class="row">
      <div class="col-lg-4 col-md-2 col-sm-2">
      </div>
      <div class="col-lg-4 col-md-8 col-sm-8 col-xs-10">
        <div class="header">
         <center><h4>Join Now</h4></center>   
        </div>
         <div class="row middle">
        <div  class="col-md-12 col-sm-12 col-xs-12">
          <br>
        <h6 >
               <center><strong> Upload a file Of your Driving License</strong><br>
                Please make sure we can easily read all the detials</center>
            </h6>
            <br>
            <center>
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="box1">
              <form role="form" method="POST" action="license" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{$id}}">
              <div class="col-md-10 col-sm-10 col-xs-10">
              <img class="img-responsive" id="file" src="image/icon.png" style="margin-top:20px; width: 100%; height: auto" />
              </div>
              
             <center>
              <br>
              <label for="license" class="custom-file-upload" style="background-color: #3f8ad2;border-radius: 10px;color:  #fff;width: 180px;cursor:  pointer;">Choose a file</label>
              
              <input type='file' name="license" id="license" class="img-responsive" style="display:none">
             </center>
            </div>
          </div></center>
        <br><br>
       </div>
               
            
        </div>
        <button type="submit" class="btn1 btn-primary btn-block" style="height: 10%" ><center><h4>Next</h4></center></button>
       </div>
       {!! csrf_field() !!}
       </form>
       </center> 
        </div>

      </div>
      </div>
      
  </div>
  <script type="text/javascript">
      $(function () {
    $(":file").change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
        }
    });
});

function imageIsLoaded(e) {
    $('#file').attr('src', e.target.result);
};
</script>


  
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </body>
</html>