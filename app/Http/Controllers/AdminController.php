<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Admin;
use Session;
use Response;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function driver(Request $request){
          $user=User::get()->paginate(5);
          return view('adminPages.driver')->with('user',$user);
        }

     public function deleteUser(Request $request){
          $user=User::findOrFail($request->id);
          $user->delete();
          $user=User::get();
          return redirect('driver');
        }

     public function detailUser(Request $request){
          $user=User::findOrFail($request->id);
          return view('adminPages.detail')->with('user', $user);
          }
public function registerAdmin(Request $request){

    if (!(Admin::where('email', '=', $request->email)->count() > 0)){        
        $admin = new Admin;
        
        $admin->email=$request->email;
        $admin->password=bcrypt($request->password);        
        $admin->save();
        $response = array(
                'status' => 'true',
                'msg' => 'registered successfully'
                        );
    }
    else{

        $response = array(
                'status' => 'error',
                'msg' => 'email already exists'
                        );
    }
    return Response::json($response);


}

public function login(Request $request){
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
        $response = array(
            'status' => true,
            //'id' => Auth::guard('admin')->user()->talent_id,   
            'msg' => 'You have been logged in successfully' 
        );
  return redirect('index');      
    }
    else{
  return view('adminPages.admin')->with('error','Invalid Credentials');
    }
}

}
