<?php

namespace App\Http\Controllers;
use App\User;
use App\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Datebase\Query\Builder;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function registerUser(Request $request){
    if (!(User::where('email', '=', $request->email)->count()) > 0){ 
        $user = new User;
        $user->firstName=$request->firstName;
        $user->lastName=$request->lastName;
        $user->email=$request->email;
        $user->password=bcrypt($request->password);
        $user->phoneNumber=$request->phoneNumber;
	    $user->city=$request->city;
        $user->code=$request->code;
        $user->save();
        $id=$user->id;
        return view('pages.driverlicense')->with('user',$user)->with('id', $id);
        }
   else{
    
    return view('pages.signUp')->with('error', 'Email already exist');
   }
        

}

    public function license(Request $request){
    	$user=User::findorfail($request->id);
    	//dd($user->id);
    	if($request->hasfile('license')){
            $hash= md5(rand(0,1000));
            $image=$request->file('license');
            $fileName=$hash.'.'.$request->file('license')->getClientOriginalExtension();
            $user->license='http://localhost/wegoo/storage/app/public/'.$fileName;
            $image->storeAs('public/',$fileName);
            $user->save();
             $id=$user->id;
             return view('pages.uber')->with('user',$user)->with('id', $id);

    }
}

}
